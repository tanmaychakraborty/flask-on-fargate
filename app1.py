from flask import Flask, jsonify, request
import joblib

app = Flask(__name__)

# Load the ML model
model = joblib.load('model.joblib')

@app.route('/predict', methods=['POST'])
def predict():
    # Get the data from the POST request
    data = request.get_json()

    prediction=model.predict([data["input"]])

    response = {
        "prediction": prediction.tolist()
    }

    return jsonify(response)

if __name__ == '__main__':
    app.run(debug=True,host="0.0.0.0",port=5000)
